/*
   90dnstester -- test the 90dns servers to make sure they work on your network
   written by pika licensed under the gnu gpl
   changed by Nolanlemahn to include additional tests and accept more-specific resolutions under the gnu gpl
   you can grab a copy of the license at https://www.gnu.org/licenses/gpl-3.0.en.html
*/

package main

import (
	"bufio"
	"fmt"
	"github.com/miekg/dns"
	"os"
	"strings"
)

func getRecord(server string, domain string) string {
	c := new(dns.Client)
	m := new(dns.Msg)
	m.SetQuestion(dns.Fqdn(domain), dns.TypeA)
	r, _, err := c.Exchange(m, server+":53")
	if err != nil {
		fmt.Printf("[error] there was an error while sending the request to the dns server:\n")
		panic(err)
	}
	
	return r.Answer[0].String()
}

func testRecord(record string, domain string, ip string) bool {
	// check for domain and ip
	return (strings.Contains(record, domain) &&
			strings.Contains(record, ip))
}

func main() {
	ips := []string{"163.172.141.219", "207.246.121.77"}
	testset := []string{"nintendo.com", "nintendo.net", "sun.hac.lp1.d4c.nintendo.net", "ctest.cdn.nintendo.net", "conntest.nintendowifi.net", "90dns.test"}
	//accept loopback and lavaDNS
	acceptableResolves := []string{"127.0.0.1", "95.216.149.205", "207.246.121.77"}
	
	for _, dns := range ips {
		successfulTests := 0
		for _, test := range testset {
			testResolved := false
			record := getRecord(dns, test)
			for _, resolution := range acceptableResolves {
				
				if testRecord(record, test, resolution) {
					successfulTests += 1
					fmt.Printf("[info] test on %s with %s succeeded! (Resolution was %s)\n", test, dns, resolution)
					testResolved = true
					break
				}
			}
			
			if !testResolved {
				//also accept the DNS itself.
				if testRecord(record, test, dns) {
					successfulTests += 1
					fmt.Printf("[info] test on %s with %s succeeded! (Resolution was %s)\n", test, dns, dns)
				} else {
					fmt.Printf("[info] test on %s with %s failed!\n", test, dns)
				}
			}
		}
		
		if successfulTests == len(testset) {
			fmt.Printf("[info] %d/%d tests succeeded!\n[RESULT] IT IS SAFE to use 90dns (%s) on this network!\n", successfulTests, len(testset), dns)
		} else {
			fmt.Printf("[info] %d/%d tests succeeded!\n[RESULT] IT IS *NOT* SAFE to use 90dns (%s) on this network!\n", successfulTests, len(testset), dns)
		}
	}

	fmt.Printf("[prompt] press enter to exit.")
	bufio.NewScanner(os.Stdin).Scan()
}
