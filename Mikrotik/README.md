Open a console on your Mikrotik router and enter the following to add the 
required configuration entries:

```
/ip dns static
add address=95.216.149.205 regexp="conntest\\.nintendowifi\\.net"
add address=95.216.149.205 regexp="ctest\\.cdn\\.nintendo\\.net"
add address=127.0.0.1 regexp=".*\\.nintendo\\..*"
add address=127.0.0.1 regexp="^nintendo\\..*"
```

Thank you @ibnux for your help with this section.